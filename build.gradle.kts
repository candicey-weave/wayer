plugins {
    kotlin("jvm") version ("1.9.23")
    id("com.github.weave-mc.weave") version ("8b70bcc707")
    id("com.github.johnrengelman.shadow") version ("8.1.1")
}

group = "com.gitlab.candicey.wayer"
version = "0.1.1"

minecraft.version("1.8.9")

repositories {
    mavenCentral()
    maven("https://repo.weavemc.dev/releases")
    maven("https://jitpack.io")
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("org.ow2.asm:asm-tree:9.4")
    implementation("com.google.code.gson:gson:2.10.1")
    implementation("net.weavemc.api:common:1.0.0-b.2")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}

tasks.jar {
    manifest {
        attributes["Main-Class"] = "com.gitlab.candicey.wayer.app.WayerAppMainKt"
    }
}