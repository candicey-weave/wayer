package net.weavemc.loader.api;

import org.apache.commons.lang3.ArrayUtils;

public abstract class Hook extends net.weavemc.api.Hook {
    public Hook() {
        super();
    }

    public Hook(String target, String... extraTargets) {
        super(ArrayUtils.add(extraTargets, target));
    }
}
