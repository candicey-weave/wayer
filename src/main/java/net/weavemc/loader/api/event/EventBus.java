package net.weavemc.loader.api.event;

import java.util.function.Consumer;

public class EventBus {
    public static void subscribe(Object obj) {
        net.weavemc.api.event.EventBus.subscribe(obj);
    }

    public static  <T extends Event> void subscribe(Class<T> event, Consumer<T> handler) {
        net.weavemc.api.event.EventBus.subscribe(event, handler);
    }

    public static <T extends Event> void callEvent(T event) {
        net.weavemc.api.event.EventBus.postEvent(event);
    }

    public static void unsubscribe(Consumer<? extends Event> consumer) {
        net.weavemc.api.event.EventBus.unsubscribe(consumer);
    }

    public static void unsubscribe(Object obj) {
        net.weavemc.api.event.EventBus.unsubscribe(obj);
    }
}
