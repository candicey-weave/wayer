package net.weavemc.loader.api;

import org.jetbrains.annotations.NotNull;

import java.lang.instrument.Instrumentation;

public interface ModInitializer extends net.weavemc.api.ModInitializer {
    @Override
    default void init() {
    }

    @Override
    default void preInit(@NotNull Instrumentation instrumentation) {
        preInit();
    }

    void preInit();
}
