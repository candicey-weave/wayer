package com.gitlab.candicey.wayer.app.gui

import com.gitlab.candicey.wayer.app.*
import java.awt.BorderLayout
import java.awt.Component
import java.awt.FlowLayout
import java.io.File
import java.util.jar.JarEntry
import java.util.jar.JarFile
import java.util.jar.JarInputStream
import java.util.jar.JarOutputStream
import javax.swing.*
import javax.swing.table.DefaultTableCellRenderer
import javax.swing.table.DefaultTableModel

class WayerFrame : JFrame() {
    private val inputDirectoryLabel = JLabel("Mods Directory")
    private val inputDirectoryField = JTextField(MODS_DIRECTORY.absolutePath)
    private val inputDirectoryButton = JButton("Choose")
    private val deleteOldMods = JCheckBox("Delete old mods").apply { isSelected = true }
    private val backupMods = JCheckBox("Backup old mods").apply { isSelected = true }
    private val recursive = JCheckBox("Recursive")
    private val outputDirectoryLabel = JLabel("Output Directory")
    private val outputDirectoryField = JTextField(File(MODS_DIRECTORY, "1.8.9").absolutePath)
    private val outputDirectoryButton = JButton("Choose")
    private val migrateButton = JButton("Migrate")
    private val tableModel = DefaultTableModel()
    private val table = JTable(tableModel)
    private val modsDirectoryPanel = JPanel(FlowLayout()).apply {
        add(inputDirectoryLabel)
        add(inputDirectoryField)
        add(inputDirectoryButton)
    }
    private val deleteOldModsPanel = JPanel(FlowLayout(FlowLayout.LEFT)).apply { add(deleteOldMods) }
    private val backupModsPanel = JPanel(FlowLayout(FlowLayout.LEFT)).apply { add(backupMods) }
    private val recursivePanel = JPanel(FlowLayout(FlowLayout.LEFT)).apply { add(recursive) }
    private val outputDirectoryPanel = JPanel(FlowLayout()).apply {
        add(outputDirectoryLabel)
        add(outputDirectoryField)
        add(outputDirectoryButton)
    }
    private val directoryPanel by lazy {
        JPanel().apply {
            layout = BoxLayout(this, BoxLayout.PAGE_AXIS)
            arrayOf(
                modsDirectoryPanel,
                deleteOldModsPanel,
                backupModsPanel,
                recursivePanel,
                Box.createVerticalStrut(20),
                outputDirectoryPanel,
                Box.createVerticalStrut(20),
            ).forEach(::add)
        }
    }

    init {
        defaultCloseOperation = EXIT_ON_CLOSE
        setSize(500, 500)
        setLocationRelativeTo(null)
        layout = BorderLayout()

        inputDirectoryButton.addActionListener { chooseInputDirectory() }
        migrateButton.addActionListener { migrateMods() }
        outputDirectoryButton.addActionListener { chooseOutputDirectory() }

        add(directoryPanel, BorderLayout.NORTH)
        add(migrateButton, BorderLayout.SOUTH)

        tableModel.addColumn("File Name")
        tableModel.addColumn("Progress")

        pack()
        isVisible = true
    }

    private fun chooseInputDirectory() {
        val chooser = JFileChooser()
            .apply {
                currentDirectory = MODS_DIRECTORY
                fileSelectionMode = JFileChooser.FILES_AND_DIRECTORIES
                isMultiSelectionEnabled = true
                isAcceptAllFileFilterUsed = false
                isFileHidingEnabled = true
            }

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            inputDirectoryField.text = chooser.selectedFiles.joinToString(CLASSPATH_SEPARATOR) { it.absolutePath }
        }
    }

    private fun chooseOutputDirectory() {
        val chooser = JFileChooser()
            .apply {
                currentDirectory = MODS_DIRECTORY
                fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
                isAcceptAllFileFilterUsed = false
            }

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            outputDirectoryField.text = chooser.selectedFile.absolutePath
        }
    }

    private fun migrateMods() {
        arrayOf(
            directoryPanel,
            migrateButton,
        ).forEach(::remove)

        add(JScrollPane(table), BorderLayout.NORTH)

        val mods = listMods()

        table.columnModel.getColumn(0).cellRenderer = FileNameCellRenderer(mods)

        for (mod in mods) {
            updateTable(mod.value.name, false)
        }

        val worker = object : SwingWorker<Unit, Unit>() {
            override fun doInBackground() {
                for (mod in mods) {
                    migrateMod(mod.value)
                    SwingUtilities.invokeLater {
                        updateTable(mod.value.name, true)
                    }
                }
            }

            override fun done() {
                for (mod in mods) {
                    if (backupMods.isSelected) {
                        backupMod(mod.value)
                    }

                    if (deleteOldMods.isSelected) {
                        deleteMod(mod.value)
                    }
                }

                println("Migration process is complete. Cloning wayer.jar...")
                cloneWayerJar()
                println("Cloning process is complete.")

                JOptionPane.showMessageDialog(this@WayerFrame, "Migration process is complete. You can now close the program.")
            }
        }

        worker.execute()

        revalidate()
        repaint()
    }

    private fun migrateMod(mod: File) {
        val outputDirectory = File(outputDirectoryField.text).apply(File::mkdirs)
        val outputFile = File(outputDirectory, "${mod.nameWithoutExtension}-migrated.jar")

        JarFile(mod).migrateAndCopyTo(outputFile)
    }

    private fun cloneWayerJar() {
        // check if it's a dev environment
        if (CURRENT_JAR_PATH.isDirectory || CURRENT_JAR_PATH.extension != "jar") {
            JOptionPane.showMessageDialog(this, "Please run the program from a jar file.")
            return
        }

        val outputDirectory = File(outputDirectoryField.text).apply(File::mkdirs)
        val outputFile = File(outputDirectory, "wayer.jar").apply {
            if (exists()) {
                delete()
            }
            createNewFile()
        }

        JarInputStream(CURRENT_JAR_PATH.inputStream()).use { jarIn ->
            JarOutputStream(outputFile.outputStream()).use { jarOut ->
                while (true) {
                    val entry = jarIn.nextJarEntry ?: break
                    if (entry.isDirectory || !(entry.name == "weave.mod.json" || entry.name.startsWith("net/weavemc/") || entry.name.startsWith("com/gitlab/candicey/wayer/mod/") || entry.name.startsWith("com/gitlab/candicey/wayer/util/"))) {
                        continue
                    }

                    jarOut.putNextEntry(entry)
                    jarIn.copyTo(jarOut)
                    jarOut.closeEntry()
                }
            }
        }
    }

    private fun backupMod(mod: File) {
        val backupFile = File(mod.parentFile, "${mod.name}.wayer-backup")
        mod.copyTo(backupFile, overwrite = true)
    }

    private fun deleteMod(mod: File) {
        mod.delete()
    }

    private fun updateTable(modName: String, isMigrated: Boolean) {
        val status = if (isMigrated) "✓" else "✗"

        if (tableModel.rowCount == 0) {
            tableModel.addRow(arrayOf(modName, status))
        } else {
            for (i in 0 ..< tableModel.rowCount) {
                if (tableModel.getValueAt(i, 0) == modName) {
                    tableModel.setValueAt(status, i, 1)
                    return
                }
            }
            tableModel.addRow(arrayOf(modName, status))
        }
    }

    private fun listMods(): Map<String, File> {
        val text = inputDirectoryField.text
        val directories = text.split(CLASSPATH_SEPARATOR)
            .map(::File)

        val mods = mutableMapOf<String, File>()
        for (directory in directories) {
            val files = if (directory.isDirectory) {
                getMods(directory, recursive.isSelected)
            } else {
                listOf(directory)
            }

            for (file in files) {
                mods[file.name] = file
            }
        }

        return mods
    }
}

class FileNameCellRenderer(private val mods: Map<String, File>) : DefaultTableCellRenderer() {
    override fun getTableCellRendererComponent(table: JTable, value: Any, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component {
        val component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column) as JLabel
        val fileName = value as String
        val file = mods[fileName]
        component.toolTipText = file?.absolutePath
        return component
    }
}