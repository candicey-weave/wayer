package com.gitlab.candicey.wayer.app

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Remapper
import java.io.File
import java.security.MessageDigest
import java.util.jar.JarFile
import java.util.jar.JarOutputStream
import java.util.zip.ZipEntry

val File.sha256: String
    get() = MessageDigest
        .getInstance("SHA-256")
        .digest(readBytes())
        .joinToString("") { "%02x".format(it) }

fun JarFile.migrateAndCopyTo(target: File) {
    val bytesMap = mutableMapOf<String, ByteArray>()
    val entries = entries()
    while (entries.hasMoreElements()) {
        val entry = entries.nextElement()
        var bytes = getInputStream(entry).readBytes()

        when {
            entry.name == "weave.mod.json" -> {
                val string = bytes.toString(Charsets.UTF_8)

                val json = GSON.fromJson(string, JsonObject::class.java)
                    .apply {
                        if (!has("name")) {
                            addProperty("name", "WeaveMod-Migrated_By_Wayer-${File(this@migrateAndCopyTo.name).sha256}")
                        }

                        if (!has("modId")) {
                            addProperty("modId", get("name").asString.replace(" ", "_").lowercase())
                        }

                        if (has("entrypoints")) {
                            add("entryPoints", get("entrypoints"))
                            remove("entrypoints")
                        } else {
                            add("entryPoints", JsonArray())
                        }

                        addProperty("namespace", "mcp-named")

                        addProperty("compiledFor", "1.8.9")
                    }

                bytes = GSON.toJson(json).toByteArray(Charsets.UTF_8)
            }

            entry.name.endsWith(".class") -> {
                bytes = bytes.remap()
            }
        }

        bytesMap[entry.name] = bytes
    }

    bytesMap["net/weavemc/loader/api/Hook.class"] = hookBytes

    target.parentFile.mkdirs()
    JarOutputStream(target.outputStream()).use {
        for ((name, bytes) in bytesMap) {
            it.putNextEntry(ZipEntry(name))
            it.write(bytes)
            it.closeEntry()
        }
    }
}

fun ByteArray.remap(): ByteArray {
    val remapper = object : Remapper() {
        override fun map(internalName: String): String? = remapList[internalName]
    }
    val classReader = ClassReader(this)
    val classWriter = ClassWriter(classReader, 0)

    classReader.accept(ClassRemapper(classWriter, remapper), 0)

    return classWriter.toByteArray()
}