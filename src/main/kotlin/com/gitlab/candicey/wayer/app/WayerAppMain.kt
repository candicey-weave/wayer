package com.gitlab.candicey.wayer.app

import com.gitlab.candicey.wayer.app.gui.WayerFrame
import javax.swing.SwingUtilities

fun main() {
    SwingUtilities.invokeLater { WayerFrame() }
}