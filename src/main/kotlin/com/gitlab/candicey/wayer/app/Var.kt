package com.gitlab.candicey.wayer.app

import com.google.gson.GsonBuilder
import net.weavemc.loader.api.event.Event
import java.io.File
import java.util.jar.JarFile

val HOME = File(System.getProperty("user.home"))
val WEAVE_DIRECTORY = File(HOME, ".weave")
val MODS_DIRECTORY = File(WEAVE_DIRECTORY, "mods")

val GSON = GsonBuilder().setPrettyPrinting().create()

val CURRENT_JAR_PATH = File(Event::class.java.protectionDomain.codeSource.location.toURI().path)

val CLASSPATH_SEPARATOR = System.getProperty("path.separator")

val hookBytes by lazy {
    JarFile(CURRENT_JAR_PATH).use {
        it.getInputStream(it.getEntry("net/weavemc/loader/api/Hook.class")).readBytes()
    }
}

/**
 * old to new
 */
val remapList = mapOf(
//    "net/weavemc/loader/api/ModInitializer" to "net/weavemc/api/ModInitializer",
//    "net/weavemc/loader/api/Hook" to "net/weavemc/api/Hook",
    "net/weavemc/loader/api/Hook\$AssemblerConfig" to "net/weavemc/api/Hook\$AssemblerConfig",
//    "net/weavemc/loader/api/event/EventBus" to "net/weavemc/api/event/EventBus",
    "net/weavemc/loader/api/event/SubscribeEvent" to "net/weavemc/api/event/SubscribeEvent",
)