package com.gitlab.candicey.wayer.app

import com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER
import java.io.File
import java.net.URL

internal fun info(message: String) = println("[Wayer] [INFO] $message")

internal fun warn(message: String) = println("[Wayer] [WARN] $message")

internal fun debug(message: String) = println("[Wayer] [DEBUG] $message")

fun downloadFile(url: String, path: String, sha256: String? = null, async: Boolean = false, success: () -> Unit = {}, failure: () -> Unit = {}) {
    val file = File(path)
    if (file.exists()) {
        if (sha256 != null && sha256 != file.sha256) {
            warn("File $path has an invalid checksum, redownloading")
            file.delete()
        } else {
            success()
            return
        }
    }

    debug("Downloading $url to $path")

    val download = {
        try {
            val bytes = URL(url).readBytes()

            if (!file.exists()) {
                file.parentFile?.mkdirs()
                file.createNewFile()
            }

            file.writeBytes(bytes)

            if (sha256 != null && sha256 != file.sha256) {
                warn("File $path has an invalid checksum, redownloading")
                file.delete()
                downloadFile(url, path, sha256, async, success, failure)
            } else {
                success()
            }
        } catch (e: Exception) {
            warn("Failed to download $url to $path")
            failure()
        }
    }

    if (async) {
        Thread(download).start()
    } else {
        download()
    }
}

fun downloadTempFile(url: String, sha256: String? = null, async: Boolean = false, success: (File) -> Unit = {}, failure: () -> Unit = {}, deleteOnExit: Boolean = true) {
    val file = createTempFile(deleteOnExit = deleteOnExit)

    downloadFile(
        url = url,
        path = file.path,
        sha256 = sha256,
        async = async,
        success = { success(file) },
        failure = {
            file.delete()
            failure()
        }
    )
}

fun createTempFile(prefix: String? = null, suffix: String? = null, deleteOnExit: Boolean = true) = File
    .createTempFile(prefix ?: "Wayer", suffix)
    .also { if (deleteOnExit) it.deleteOnExit() }

fun getMods(directory: File = MODS_DIRECTORY, recursive: Boolean = false): List<File> =
    if (recursive) {
        directory.walkTopDown().filter { it.extension == "jar" }.toList()
    } else {
        directory.listFiles { _, name -> name.endsWith(".jar") }?.toList() ?: emptyList()
    }