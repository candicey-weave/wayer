package com.gitlab.candicey.wayer.mod.hook

import com.gitlab.candicey.wayer.util.callEvent
import com.gitlab.candicey.wayer.util.getSingleton
import com.gitlab.candicey.wayer.util.named
import net.weavemc.api.Hook
import net.weavemc.loader.api.event.ShutdownEvent
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode

/**
 * Corresponds to [ShutdownEvent].
 */
class ShutdownEventHook : Hook("net/minecraft/client/Minecraft") {
    /**
     * Inserts a call to
     * [net.minecraft.client.Minecraft.shutdownMinecraftApplet].
     * at the head of [net.minecraft.client.Minecraft.shutdownMinecraftApplet].
     *
     * @see net.minecraft.client.Minecraft.shutdownMinecraftApplet
     */
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods.named("shutdownMinecraftApplet").instructions.insert(asm {
            getSingleton<ShutdownEvent>()
            callEvent()
        })
    }
}
