package com.gitlab.candicey.wayer.mod.hook

import com.gitlab.candicey.wayer.util.callEvent
import com.gitlab.candicey.wayer.util.getSingleton
import com.gitlab.candicey.wayer.util.named
import net.weavemc.api.Hook
import net.weavemc.loader.api.event.StartGameEvent
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes.RETURN
import org.objectweb.asm.tree.ClassNode

/**
 * Corresponds to [StartGameEvent.Pre] and [StartGameEvent.Post].
 */
class StartGameEventHook : Hook("net/minecraft/client/Minecraft") {
    /**
     * Inserts a call to [net.minecraft.client.Minecraft.startGame] using the Event Bus.
     *
     * [StartGameEvent.Pre] is called at the head of [net.minecraft.client.Minecraft.startGame]. Whereas
     * [StartGameEvent.Post] is called at the tail of [net.minecraft.client.Minecraft.startGame].
     *
     * @see net.minecraft.client.Minecraft.startGame
     */
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val mn = node.methods.named("startGame")

        mn.instructions.insert(asm {
            getSingleton<StartGameEvent.Pre>()
            callEvent()
        })

        mn.instructions.insertBefore(mn.instructions.findLast { it.opcode == RETURN }, asm {
            getSingleton<StartGameEvent.Post>()
            callEvent()
        })
    }
}
