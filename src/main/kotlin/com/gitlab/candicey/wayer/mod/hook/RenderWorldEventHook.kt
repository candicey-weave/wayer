package com.gitlab.candicey.wayer.mod.hook

import com.gitlab.candicey.wayer.util.callEvent
import com.gitlab.candicey.wayer.util.internalNameOf
import com.gitlab.candicey.wayer.util.named
import net.weavemc.api.Hook
import net.weavemc.loader.api.event.RenderWorldEvent
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LdcInsnNode

/**
 * Corresponds to [RenderWorldEvent].
 */
class RenderWorldEventHook : Hook("net/minecraft/client/renderer/EntityRenderer") {
    /**
     * Inserts a call to [RenderWorldEvent]'s constructor at the head of
     * [net.minecraft.client.renderer.EntityRenderer.renderWorldPass], which
     * is called in the event of any world render.
     */
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val mn = node.methods.named("renderWorldPass")

        mn.instructions.insertBefore(
            mn.instructions.find { it is LdcInsnNode && it.cst == "hand" },
            asm {
                new(internalNameOf<RenderWorldEvent>())
                dup
                fload(2)
                invokespecial(internalNameOf<RenderWorldEvent>(), "<init>", "(F)V")
                callEvent()
            }
        )
    }
}
