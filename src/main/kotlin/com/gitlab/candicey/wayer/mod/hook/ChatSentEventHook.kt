package com.gitlab.candicey.wayer.mod.hook

import com.gitlab.candicey.wayer.util.callEvent
import com.gitlab.candicey.wayer.util.internalNameOf
import com.gitlab.candicey.wayer.util.named
import net.weavemc.api.Hook
import net.weavemc.api.event.CancellableEvent
import net.weavemc.loader.api.event.ChatSentEvent
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LabelNode

class ChatSentEventHook : Hook("net/minecraft/client/entity/EntityPlayerSP") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods.named("sendChatMessage").instructions.insert(asm {
            new(internalNameOf<ChatSentEvent>())
            dup
            dup
            aload(1)
            invokespecial(
                internalNameOf<ChatSentEvent>(),
                "<init>",
                "(L${internalNameOf<String>()};)V"
            )
            callEvent()

            val end = LabelNode()

            invokevirtual(internalNameOf<CancellableEvent>(), "isCancelled", "()Z")
            ifeq(end)

            _return

            +end
            f_same()
        })
    }
}
