# Wayer
- Double click to migrate legacy Weave mods to Weave 1.0 

Note: Some mods may not work as expected. Feel free to contact me if you have any issues.

## Download
- [Releases](https://gitlab.com/candicey-weave/wayer/-/releases)